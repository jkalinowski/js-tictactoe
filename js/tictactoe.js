let gameArea = [
  ['', '', ''],
  ['', '', ''],
  ['', '', ''],
];
let availableMoves = [];

let players = ['X', 'O'];
let currentPlayer;

function isNumberBetweenRange(num, min, max) {
  return num >= min && num <= max;
}

function areValuesEqual(a, b, c) {
  return a == b && b == c && a != '';
}

function setup() {
  createCanvas(400, 400);

  currentPlayer = floor(random(players.length));

  for(let i = 0; i < 3; i++) {
    for(let k = 0; k < 3; k++) {
      availableMoves.push([i, k]);
    }
  }
}

function draw() {
  let w = width / 3;
  let h = height / 3;

  for(let i = 0; i < 3; i++) {
    for(let k = 0; k < 3; k++) {
      let spot = gameArea[i][k];
      let x = w * i;
      let y = h * k;
      let text_x = x + w/2;
      let text_y = y + h/2;

      rect(x, y, w, h);

      textSize(42);
      textAlign(CENTER);
      text(spot, text_x, text_y);
    }
  }

  let winner = checkWinner();
  if(winner != null) {
    noLoop();

    let resultElement;
    if(winner == 'no_moves') {
      resultElement = createP('No one wins!');
    }else {
      resultElement = createP(`The winner is: ${winner}`);
    }

    resultElement.style('font-size', '40pt');
  }
}

function nextTurn() {
  if(currentPlayer+1 < players.length) {
    currentPlayer++;
  }else {
    currentPlayer = 0;
  }
  let index = floor(random(availableMoves.length));
  availableMoves.splice(index, 1)[0];
}

function checkWinner() {
  let winner = null;

  // Horizontal
  for(let i = 0; i < 3; i++) {
    if(areValuesEqual(gameArea[i][0], gameArea[i][1], gameArea[i][2])) {
      winner = gameArea[i][0];
    }
  }

  // Vertical
  for(let i = 0; i < 3; i++) {
    if(areValuesEqual(gameArea[0][i], gameArea[1][i], gameArea[2][i])) {
      winner = gameArea[0][i];
    }
  }

  // Diagonal
  if(areValuesEqual(gameArea[0][0], gameArea[1][1], gameArea[2][2])) {
    winner = gameArea[0][0];
  }

  if(areValuesEqual(gameArea[2][0], gameArea[1][1], gameArea[0][2])) {
    winner = gameArea[2][0];
  }

  //check if there is some winner
  if(winner == null && availableMoves.length == 0) {
    return 'no_moves';
  }else {
    return winner;
  }
}

function mouseClicked() {
  let w = width / 3;
  let h = height / 3;

  for(let i = 0; i < 3; i++) {
    for(let k = 0; k < 3; k++) {
      let spot = gameArea[i][k];
      let x = w * i;
      let y = h * k;
      if(isNumberBetweenRange(event.offsetX, x, x+w) && isNumberBetweenRange(event.offsetY, y, y+h)) {
        if(gameArea[i][k] == '') {
          gameArea[i][k] = players[currentPlayer];
          nextTurn();
        }
      }
    }
  }
}
